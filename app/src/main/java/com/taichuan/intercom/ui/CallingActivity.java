package com.taichuan.intercom.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taichuan.api.TCAVControl;
import com.taichuan.api.TCCall;
import com.taichuan.intercom.R;
import com.taichuan.intercom.ui.base.BaseActivity;
import com.taichuan.intercom.utils.ActionTable;
import com.taichuan.intercom.utils.RingtonePlayer;
import com.taichuan.intercom.utils.WackupManager;
import com.taichuan.net.model.TypeCallModel;
import com.taichuan.net.protocol.type.CallParameters;
import com.taichuan.net.protocol.type.DeviceTypeCommunication;

/**
 * Created by ZYB on 2016/11/17.
 */

public class CallingActivity extends BaseActivity implements SurfaceHolder.Callback {
    private static final String EXTRA_CALL_MODEL = "extra_call_model";

    private SurfaceView mSurfaceView;

    private LinearLayout mUnlockBtn = null;
    private Button mTalkHangUpBtn = null;
    private TextView mHeaderTxt = null;

    private TypeCallModel mTypeCallModel = null;

    private boolean isTalk = false;

    private int widthPixels;
    private int heightPixels;

    public static void start(Context context, TypeCallModel typeCallModel) {
        CallParameters.CALL_BUSY = true;
        Intent intent = new Intent(context, CallingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra(EXTRA_CALL_MODEL, typeCallModel);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_vi_port);
        initWidth();
        loadData();
        initView();
        initListener();
        TCAVControl.initVideoConfig(mTypeCallModel, widthPixels, heightPixels);
        regBrocast();
    }

    private void loadData() {
        mTypeCallModel = getIntent().getParcelableExtra(EXTRA_CALL_MODEL);
    }

    private void initWidth() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        widthPixels = dm.widthPixels;
        heightPixels = widthPixels * 480 / 640;
    }

    private void initView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.surface);
        mUnlockBtn = (LinearLayout) findViewById(R.id.vi_panel_opendoorlayout);
        mTalkHangUpBtn = (Button) findViewById(R.id.vi_panel_jietingguaduan);
        mHeaderTxt = (TextView) findViewById(R.id.header_text);

        if (mTypeCallModel.isIncomming() || mTypeCallModel.getCallType() == DeviceTypeCommunication.TYPE_DOOR) {
            mTalkHangUpBtn.setEnabled(true);
        }
        if (mTypeCallModel.getCallType() != DeviceTypeCommunication.TYPE_DOOR && mTypeCallModel.getCallType() != DeviceTypeCommunication.TYPE_FDOOR) {
            mUnlockBtn.setEnabled(false);
        }
    }

    private void initListener() {
        mUnlockBtn.setOnClickListener(mOnClickListener);
        mTalkHangUpBtn.setOnClickListener(mOnClickListener);
        mSurfaceView.getHolder().addCallback(this);
        mSurfaceView.getLayoutParams().width = widthPixels;
        mSurfaceView.getLayoutParams().height = heightPixels;
    }


    public View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.vi_panel_opendoorlayout:
                    TCCall.unlockDoor(mTypeCallModel);
                    break;
                case R.id.vi_panel_jietingguaduan:
                    if (!isTalk)
                        beginTalk(true);
                    else
                        hangup(true);
                    break;
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            hangup(true);
        }
        return super.onKeyDown(keyCode, event);

    }

    public void beginTalk(boolean sendCmd) {
        if (!isTalk) {
            isTalk = true;
            if (sendCmd) {
                TCCall.beginTalk(mTypeCallModel);
            }
            RingtonePlayer.stopPlayer();
            TCAVControl.play();
            mHeaderTxt.setText(getString(R.string.zheng_zai_yu) + mTypeCallModel.getRoomName() + getString(R.string.tonghuazhong));
            mTalkHangUpBtn.setText(getString(R.string.gua_duan));
            mTalkHangUpBtn.setBackgroundResource(R.drawable.guaji);
            mTalkHangUpBtn.setEnabled(true);
        }
    }

    private void hangup(boolean sendCmd) {
        if (sendCmd) {
            TCCall.hangeUp(mTypeCallModel);
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RingtonePlayer.stopPlayer();
        TCAVControl.destroy();
        unRegBroadcast();
        WackupManager.releaseLock();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        TCAVControl.surfaceCreated(surfaceHolder, widthPixels, heightPixels);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        TCAVControl.surfaceDestroyed();
    }

    private void regBrocast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ActionTable.ACTION_CALL_TALK);
        intentFilter.addAction(ActionTable.ACTION_CALL_HANGUP);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ActionTable.ACTION_CALL_TALK.equals(intent.getAction())) {
                beginTalk(false);
            } else if (ActionTable.ACTION_CALL_HANGUP.equals(intent.getAction())) {
                int status = intent.getIntExtra(ActionTable.EXTRA_CALL_HANGUP, 0);
                hangup(status == 1);
            }
        }
    };

    private void unRegBroadcast() {
        unregisterReceiver(mBroadcastReceiver);
    }
}
