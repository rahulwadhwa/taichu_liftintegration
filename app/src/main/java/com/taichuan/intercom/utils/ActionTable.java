package com.taichuan.intercom.utils;

/**
 * Created by ZYB on 2016/11/17.
 */

public class ActionTable {
    /**
     * 呼叫失败
     */
    public static final String ACTION_CALL_FAILED = "com.taichuan.intercom.aciton_call_failed";
    /**
     * 呼叫失败状态值
     */
    public static final String EXTRA_CALL_FATLED_STATUS = "extra_call_fatled_status";

    /**
     * 呼叫成功
     */
    public static final String ACTION_CALL_SUCCESS = "com.taichuan.intercom.action_call_success";

    /**
     * 对方接听
     */
    public static final String ACTION_CALL_TALK = "com.taichuan.intercom.action_call_talk";
    /**
     * 挂机
     */
    public static final String ACTION_CALL_HANGUP = "com.taichuan.intercom.action_call_hangup";

    /**
     * 挂机状态
     */
    public static final String EXTRA_CALL_HANGUP = "com.taichuan.intercom.extra_call_hangup";
}
