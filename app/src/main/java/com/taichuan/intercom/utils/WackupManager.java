package com.taichuan.intercom.utils;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;


public class WackupManager {
    // 电源管理
    private static PowerManager pManager = null;
    private static PowerManager.WakeLock mWakeLock = null;

    // 锁屏管理
    private static KeyguardManager mKeyguardManager = null;
    @SuppressWarnings("deprecation")
    private static KeyguardManager.KeyguardLock mKeyguardLock = null;


    public static void initLock(Context context) {
        // 获取系统休眠与锁屏服务
        pManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mKeyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
    }


    // 唤醒系统与解锁
    @SuppressWarnings("deprecation")
    public static void acquireLock() {
        if (mWakeLock != null)
            releaseLock();
        mWakeLock = pManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "VideoPhoneTag");
        mWakeLock.acquire();

        mKeyguardLock = mKeyguardManager.newKeyguardLock("VideoPhoneTag");
        mKeyguardLock.disableKeyguard();
    }

    // 释放休眠与锁屏
    @SuppressWarnings("deprecation")
    public static void releaseLock() {
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }

        if (mKeyguardLock != null) {
            mKeyguardLock.reenableKeyguard();
            mKeyguardLock = null;
        }
    }

}
