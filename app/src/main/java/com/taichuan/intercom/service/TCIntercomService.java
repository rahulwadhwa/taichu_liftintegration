package com.taichuan.intercom.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;

import com.taichuan.api.TCCallListener;
import com.taichuan.api.TCService;
import com.taichuan.intercom.ui.CallingActivity;
import com.taichuan.intercom.utils.ActionTable;
import com.taichuan.intercom.utils.RingtonePlayer;
import com.taichuan.intercom.utils.WackupManager;
import com.taichuan.net.model.TCMessage;
import com.taichuan.net.model.TypeCallModel;
import com.taichuan.net.protocol.type.DeviceTypeCommunication;
import com.taichuan.util.CommonUtils;

/**
 * Created by ZYB on 2016/11/14.
 */

public class TCIntercomService extends Service implements TCCallListener {

    @Override
    public void onCreate() {
        super.onCreate();
        String imei = CommonUtils.getIMEI(this);
        imei = "1223333";
        TCService.init(this.getApplicationContext(), imei);
        TCService.addTCIntercomListener(this);
        WackupManager.initLock(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onMessage(TCMessage message) {

    }

    @Override
    public void onIncomingCall(TypeCallModel typeCallModel) {
        WackupManager.acquireLock();
        // 铃声播放
        RingtonePlayer.stopPlayer();
        RingtonePlayer.initPlayer(this);
        RingtonePlayer.startPlayer();
        CallingActivity.start(this, typeCallModel);
    }

    @Override
    public void onAcceptCall(TypeCallModel typeCallModel) {
        if (typeCallModel.getCallType() != DeviceTypeCommunication.TYPE_DOOR && typeCallModel.getCallType() != DeviceTypeCommunication.TYPE_FDOOR) {
            // 铃声播放
//            RingtonePlayer.stopPlayer();
//            RingtonePlayer.initPlayer(this);
//            RingtonePlayer.startPlayer();
        }
        Intent intent = new Intent(ActionTable.ACTION_CALL_SUCCESS);
        sendBroadcast(intent);
        CallingActivity.start(this, typeCallModel);
    }

    @Override
    public void onAnswer() {
        RingtonePlayer.stopPlayer();
        sendBroadcast(new Intent(ActionTable.ACTION_CALL_TALK));
    }

    @Override
    public void onCallFail(int status) {
        Intent intent = new Intent(ActionTable.ACTION_CALL_FAILED);
        intent.putExtra(ActionTable.EXTRA_CALL_FATLED_STATUS, status);
        sendBroadcast(intent);
    }

    /**
     * @param status =0 对方挂机 ，=1已方挂机需发送挂机命令
     */
    @Override
    public void onHungup(int status) {
        RingtonePlayer.stopPlayer();
        Intent intent = new Intent(ActionTable.ACTION_CALL_HANGUP);
        intent.putExtra(ActionTable.EXTRA_CALL_HANGUP, status);
        sendBroadcast(intent);
    }
}
