package com.taichuan.intercom.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taichuan.api.TCService;
import com.taichuan.intercom.R;
import com.taichuan.intercom.ui.base.BaseActivity;
import com.taichuan.intercom.utils.DefaultToast;

public class CallSettingActivity extends BaseActivity {
    public static final int T_BIND = 1;
    public static final int T_UNBIND = 2;
    private LinearLayout mDDNSLayout, mRoomNameLayout, mHostLayout, mAboutLayout;
    private TextView mTextRoomName;
    private ImageView mBackImage;

    private ProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_setting);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(true);
        initView();
    }

    private void initView() {
        mRoomNameLayout = (LinearLayout) findViewById(R.id.set_roomname_layout);
        mAboutLayout = (LinearLayout) findViewById(R.id.set_about_layout);
        mHostLayout = (LinearLayout) findViewById(R.id.set_host_layout);
        mDDNSLayout = (LinearLayout) findViewById(R.id.show_ddns_layout);

        mTextRoomName = (TextView) findViewById(R.id.set_roomname);
        mBackImage = (ImageView) findViewById(R.id.bottom_back_image);

        mDDNSLayout.setOnClickListener(mOnClickListener);
        mBackImage.setOnClickListener(mOnClickListener);
        mHostLayout.setOnClickListener(mOnClickListener);
        mAboutLayout.setOnClickListener(mOnClickListener);
    }

    private void initData() {
        if (TCService.isConfigured()) {
            if (mRoomNameLayout.getVisibility() == View.GONE) {
                mRoomNameLayout.setVisibility(View.VISIBLE);
            }
            String name = TCService.getDeviceInfo().getRoomName();
            mTextRoomName.setText(name);
        } else {
            mRoomNameLayout.setVisibility(View.GONE);
        }
    }

    private OnClickListener mOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.show_ddns_layout:
                    Intent intent = new Intent(CallSettingActivity.this, SettingEditActivity.class);
                    startActivity(intent);
                    break;
                case R.id.bottom_back_image:
                    finish();
                    break;
                case R.id.set_host_layout:
                    if (TextUtils.isEmpty(TCService.getServerIp())) {
                        DefaultToast.showToast(getApplicationContext(), getString(R.string.ddns_project));
                        return;
                    }
                    Intent intent1 = new Intent(CallSettingActivity.this, SetHostHostActivity.class);
                    startActivity(intent1);
//                    excuteHostAddressConfig();
                    break;

                case R.id.set_about_layout:
//                    Intent intent5 = new Intent(CallSettingActivity.this, AboutActivity.class);
//                    startActivity(intent5);
                    break;
                default:
                    break;
            }

        }
    };

    @Override
    protected void onResume() {
        initData();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    // 关闭查询住户进度框
    private void closeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    // 显示进度框
    private void showprogressDialog() {
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }


}
