package com.taichuan.intercom.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.taichuan.api.TCConfigListener;
import com.taichuan.api.TCService;
import com.taichuan.intercom.R;
import com.taichuan.intercom.ui.adapter.CommonAdapter;
import com.taichuan.intercom.ui.adapter.CommonViewHolder;
import com.taichuan.intercom.ui.base.BaseActivity;
import com.taichuan.intercom.utils.DefaultToast;
import com.taichuan.net.protocol.type.AddressType;

import java.util.List;

/**
 * Created by ZYB on 2016/11/16.
 */

public class SetHostHostActivity extends BaseActivity implements TCConfigListener {
    private GridView mGridView = null;
    private TextView mTitle;
    private CommonAdapter<String> mStringCommonAdapter = null;
    private ProgressDialog mProgressDialog = null;

    private int mType = 0;
    private List<String> mInfos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_device_list);
        iniView();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(true);
        /**
         * 开始配置
         */
        TCService.startConfig(this);
        initListener();
        showprogressDialog();
    }

    private void iniView() {
        mGridView = (GridView) findViewById(R.id.layout_gridview);
        mTitle = (TextView) findViewById(R.id.header_text);
        mTitle.setText(getString(R.string.project_host));
    }

    // 关闭查询住户进度框
    private void closeProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    // 显示进度框
    private void showprogressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }
            }
        });

    }

    private void initListener() {
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showprogressDialog();
                String num = (String) adapterView.getAdapter().getItem(i);
                /**
                 * 配置下一层
                 */
                TCService.configNext(num);
            }
        });
    }

    @Override
    protected void onDestroy() {
        /**
         * 结束配置
         */
        TCService.endConfig();
        super.onDestroy();
    }

    public void onXmlClick(View view) {
        switch (view.getId()) {
            case R.id.bottom_back_image:
                finish();
                break;
        }
    }

    @Override
    public void onReturnConfigAddr(int type, int count, final List<String> infos) {
        closeProgressDialog();
        mType = type;
        mInfos = infos;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mStringCommonAdapter == null) {
                    mStringCommonAdapter = new CommonAdapter<String>(SetHostHostActivity.this, mInfos, R.layout.item_device) {
                        @Override
                        public void convert(CommonViewHolder commonViewHolder, int position, String item) {
                            TextView textView = commonViewHolder.getView(R.id.tv_name);
                            textView.setText(item + getText(mType));
                        }

                        public String getText(int mHostType) {
                            // 解析信息类型
                            String text = "";
                            switch (mHostType) {
                                case AddressType.DEVADDR_QH:
                                    text = getResources().getString(R.string.text_qi);
                                    break;
                                case AddressType.DEVADDR_DH:
                                    text = getResources().getString(R.string.text_dong);
                                    break;
                                case AddressType.DEVADDR_DYH:
                                    text = getResources().getString(R.string.text_dan_yuan);
                                    break;
                                case AddressType.DEVADDR_CH:
                                    text = getResources().getString(R.string.text_ceng);
                                    break;
                                case AddressType.DEVADDR_FH:
                                    text = getResources().getString(R.string.text_fang);
                                    break;
                                case AddressType.DEVADDR_FJH:
                                    text = getResources().getString(R.string.text_fen_ji);
                                    break;
                                default:
                                    break;
                            }
                            return text;
                        }
                    };
                    mGridView.setAdapter(mStringCommonAdapter);
                } else {
                    mStringCommonAdapter.notifyData(mInfos);
                }
            }
        });

    }

    @Override
    public void onDeviceConfigSuccess() {
        closeProgressDialog();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DefaultToast.showToast(SetHostHostActivity.this, R.string.project_success);
            }
        });
        finish();
    }

    @Override
    public void onConfigTimeOut() {
        closeProgressDialog();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DefaultToast.showToast(SetHostHostActivity.this, R.string.project_time_out);
            }
        });
        finish();
    }
}
