package com.taichuan.intercom.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.taichuan.api.TCCall;
import com.taichuan.api.TCService;
import com.taichuan.api.TCgetDeviceListListener;
import com.taichuan.intercom.R;
import com.taichuan.intercom.ui.adapter.CommonAdapter;
import com.taichuan.intercom.ui.adapter.CommonViewHolder;
import com.taichuan.intercom.ui.base.BaseActivity;
import com.taichuan.intercom.utils.ActionTable;
import com.taichuan.intercom.utils.DefaultToast;
import com.taichuan.net.model.RetInfo;
import com.taichuan.net.protocol.type.DeviceTypeCommunication;

public class CallCenterOrDoorActivity extends BaseActivity implements TCgetDeviceListListener {
    private static final String EXTRA_TYPE = "extra_type";
    private static final String EXTRA_OPERATE_TYPE = "extra_operate_type";

    private GridView mGridView;
    private CommonAdapter<RetInfo> mRetInfoCommonAdapter = null;
    private TextView mHeaderText;
    private ImageView mBottomBackImage;

    private ProgressDialog mProgressDialog = null;

    // 返回门口机类型
    private int mDevType = 1;

    /**
     * 操作类型：
     * 0：呼叫
     * 1：开锁
     */
    private int mOperatingType = 0;

    private List<RetInfo> mRetInfoList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_device_list);
        initData();
        initView();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(true);

        TCService.getDeviceInfoList(mDevType, this);
        showprogressDialog();
        regBrocast();
    }

    public static void start(Context context, int type, int operatingType) {
        Intent intent = new Intent(context, CallCenterOrDoorActivity.class);
        intent.putExtra(EXTRA_TYPE, type);
        intent.putExtra(EXTRA_OPERATE_TYPE, operatingType);
        context.startActivity(intent);
    }

    private void initData() {
        Intent intent = getIntent();
        mDevType = intent.getIntExtra(EXTRA_TYPE, DeviceTypeCommunication.DeviceInfoType.TYPE_DOOR);
        mOperatingType = intent.getIntExtra(EXTRA_OPERATE_TYPE, 0);
    }

    private void initView() {
        mGridView = (GridView) findViewById(R.id.layout_gridview);
        mHeaderText = (TextView) findViewById(R.id.header_text);
        mBottomBackImage = (ImageView) findViewById(R.id.bottom_back_image);

        if (mDevType == DeviceTypeCommunication.DeviceInfoType.TYPE_CENTER)
            mHeaderText.setText(getString(R.string.guan_li_zhong_xin_lie_biao));
        else if (mDevType == DeviceTypeCommunication.DeviceInfoType.TYPE_DOOR)
            mHeaderText.setText(getString(R.string.jian_kong_men_kou_ji_lie_biao));

        mGridView.setOnItemClickListener(mOnItemClickListener);
        mBottomBackImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            RetInfo info = (RetInfo) parent.getAdapter().getItem(position);
            if (mDevType == DeviceTypeCommunication.DeviceInfoType.TYPE_DOOR) {
                if (mOperatingType == 0) {
                    TCCall.callDoor(info);
                } else if (mOperatingType == 1) {
                    TCCall.unlockDoor(info);
                    DefaultToast.showToast(CallCenterOrDoorActivity.this, R.string.kai_suo);
                }
            } else if (mDevType == DeviceTypeCommunication.DeviceInfoType.TYPE_CENTER) {
                TCCall.callCenter(info);
            }
            if (mOperatingType != 1)
                showprogressDialog();
        }
    };

    @Override
    protected void onDestroy() {
        TCService.endGetDeviceInfoList();
        unRegBrocast();
        super.onDestroy();
    }

    private void showToatOnMainThread() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                DefaultToast.showToast(getApplicationContext(),
                        getString(R.string.cannot_get_data));
            }
        });
    }

    // 关闭查询住户进度框
    private void closeProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    // 显示进度框
    private void showprogressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }
            }
        });

    }

    private void regBrocast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ActionTable.ACTION_CALL_FAILED);
        intentFilter.addAction(ActionTable.ACTION_CALL_SUCCESS);
        registerReceiver(mReceiver, intentFilter);
    }

    private void unRegBrocast() {
        unregisterReceiver(mReceiver);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ActionTable.ACTION_CALL_FAILED.equals(intent.getAction())) {
                closeProgressDialog();
                int failedStatus = intent.getIntExtra(ActionTable.EXTRA_CALL_FATLED_STATUS, 0);
                if (failedStatus == 1) {
                    DefaultToast.showToast(context, R.string.hu_jiao_shi_bai);
                } else if (failedStatus == 0) {
                    DefaultToast.showToast(context, R.string.dui_fang_zheng_mang);
                }
            } else if (ActionTable.ACTION_CALL_SUCCESS.equals(intent.getAction())) {
                closeProgressDialog();
            }
        }
    };

    @Override
    public void onAllDeviceLists(int type, ArrayList<RetInfo> infos) {
        mRetInfoList = infos;
        closeProgressDialog();
        if (infos.size() == 0) {
            finish();
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRetInfoCommonAdapter == null) {
                    mRetInfoCommonAdapter = new CommonAdapter<RetInfo>(CallCenterOrDoorActivity.this, mRetInfoList, R.layout.item_device) {

                        @Override
                        public void convert(CommonViewHolder commonViewHolder, int position, RetInfo item) {
                            TextView textView = commonViewHolder.getView(R.id.tv_name);
                            if (mDevType == DeviceTypeCommunication.DeviceInfoType.TYPE_CENTER) {
                                textView.setBackgroundResource(R.drawable.btn_manage_name_style);
                            } else if (mDevType == DeviceTypeCommunication.DeviceInfoType.TYPE_DOOR) {
                                textView.setBackgroundResource(R.drawable.btn_gate_name_style);
                            }
                            textView.setText(item.getName());
                        }
                    };
                    mGridView.setAdapter(mRetInfoCommonAdapter);
                } else {
                    mRetInfoCommonAdapter.notifyData(mRetInfoList);
                }
            }
        });
    }

    public void onXmlClick(View view) {
        switch (view.getId()) {
            case R.id.bottom_back_image:
                finish();
        }
    }

    @Override
    public void onGetListTimerOut() {
        closeProgressDialog();
        finish();
    }
}