package com.taichuan.intercom.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;

public class RingtonePlayer {
    private static MediaPlayer player = null;

    public static void initPlayer(Context context) {
        try {
            setSpeakerphoneOn(context);//开最大声音
            player = new MediaPlayer();
            player.setDataSource(context, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));
            player.setAudioStreamType(AudioManager.STREAM_RING);
            player.setLooping(true);
            player.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //一直反应音量小
    public static void setSpeakerphoneOn(Context context) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        //通话时设置免提System.out.println("isSpeakerphoneOn =" + audioManager.isSpeakerphoneOn());
        mAudioManager.setSpeakerphoneOn(true);
//        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0); //tempVolume:音量绝对值
//        mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, maxVolume, 0); //tempVolume:音量绝对值
//        mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, maxVolume, 0); //tempVolume:音量绝对值

    }

    public static void startPlayer() {
        if (player != null && !player.isPlaying()) {
            player.start();
        }
    }

    public static void stopPlayer() {
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
                player.release();
                player = null;
            }
        }
    }
}