package com.taichuan.intercom.ui.base;

import android.app.Application;
import android.content.Intent;

import com.taichuan.intercom.service.TCIntercomService;

public class ApplicationIT extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Intent intent = new Intent(this, TCIntercomService.class);
        startService(intent);
    }

}
