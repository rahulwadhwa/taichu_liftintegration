package com.taichuan.intercom.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.taichuan.api.TCService;
import com.taichuan.intercom.R;
import com.taichuan.intercom.ui.base.BaseActivity;
import com.taichuan.intercom.utils.DefaultToast;
import com.taichuan.net.protocol.type.DeviceTypeCommunication;

public class MainActivity extends BaseActivity {
    private ImageView mMk1, mMk2, mMk3, mMk4, mKaiSuo;

    private ProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        initView();
        initListener();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(true);
    }

    private void initView() {
        mMk1 = (ImageView) findViewById(R.id.mokuai1);
        mMk2 = (ImageView) findViewById(R.id.mokuai2);
        mMk3 = (ImageView) findViewById(R.id.mokuai3);
        mMk4 = (ImageView) findViewById(R.id.mokuai4);
        mKaiSuo = (ImageView) findViewById(R.id.kaisuo);
    }

    private void initListener() {
        mMk1.setOnClickListener(mOnClickListener);
        mMk2.setOnClickListener(mOnClickListener);
        mMk3.setOnClickListener(mOnClickListener);
        mMk4.setOnClickListener(mOnClickListener);
        mKaiSuo.setOnClickListener(mOnClickListener);
    }

    private OnClickListener mOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = null;
            switch (v.getId()) {
                case R.id.mokuai1:
                    break;
                case R.id.mokuai2:
                    if (!TCService.isConfigured()) {
                        DefaultToast.showToast(getApplicationContext(), R.string.first_host_project);
                        return;
                    }
                    CallCenterOrDoorActivity.start(MainActivity.this, DeviceTypeCommunication.DeviceInfoType.TYPE_CENTER, 0);
                    break;
                case R.id.mokuai3:
                    if (!TCService.isConfigured()) {
                        DefaultToast.showToast(getApplicationContext(), R.string.first_host_project);
                        return;
                    }
                    CallCenterOrDoorActivity.start(MainActivity.this, DeviceTypeCommunication.DeviceInfoType.TYPE_DOOR, 0);
                    break;
                case R.id.mokuai4:
                    intent = new Intent(MainActivity.this,
                            CallSettingActivity.class);
                    startActivityForResult(intent, 0);
                    break;
                case R.id.kaisuo:
                    if (!TCService.isConfigured()) {
                        DefaultToast.showToast(getApplicationContext(), R.string.first_host_project);
                        return;
                    }
                    CallCenterOrDoorActivity.start(MainActivity.this, DeviceTypeCommunication.DeviceInfoType.TYPE_DOOR, 1);
                    break;
                default:
                    break;
            }

        }
    };


}
