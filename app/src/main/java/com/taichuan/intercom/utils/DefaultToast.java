package com.taichuan.intercom.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by ZYB on 2016/11/16.
 */

public class DefaultToast {

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, int msgId) {
        Toast.makeText(context, context.getString(msgId), Toast.LENGTH_SHORT).show();
    }
}
