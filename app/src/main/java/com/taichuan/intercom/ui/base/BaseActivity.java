package com.taichuan.intercom.ui.base;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class BaseActivity extends FragmentActivity {
    public static final String DISMISSDIALOG_ACTION = "com.taichuan.intercom.videophone_dismissdialog_action";
    public static final String STOPTIMER_ACTION = "com.taichuan.intercom.videophone_stoptimer_action";
    public static final String DOORCENTERLISTINFO_ACTION = "com.taichuan.intercom.videophone_doorcenterlistinfo_action";
    public static final String CALL_FAILURE = "com.taichuan.intercom.videophone_call_failure_action";

    public static final String INTENT_DCLIST = "intent_dclist";
    public static final String INTENT_DCDEVICETYPE = "intent_dcdevicetype";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
