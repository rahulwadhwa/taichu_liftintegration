package com.taichuan.intercom.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.taichuan.api.TCService;
import com.taichuan.intercom.R;
import com.taichuan.intercom.ui.base.BaseActivity;
import com.taichuan.intercom.utils.DefaultToast;
import com.taichuan.util.CommonUtils;

public class SettingEditActivity extends BaseActivity {

    private EditText mEditText;
    private Button mSure;
    private ImageView mBackImage;
    private TextView mHeaderText;


    private ProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_setting_edit);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(true);
        init();
    }


    private void init() {
        mEditText = (EditText) findViewById(R.id.layout_edit);
        mSure = (Button) findViewById(R.id.layout_sure);
        mBackImage = (ImageView) findViewById(R.id.bottom_back_image);
        mHeaderText = (TextView) findViewById(R.id.header_text);

        mBackImage.setOnClickListener(mOnClickListener);
        mSure.setOnClickListener(mOnClickListener);

        mHeaderText.setText(getString(R.string.ddns_ip_setting));
        String ddnsIp = TCService.getServerIp();
        if (!TextUtils.isEmpty(ddnsIp)) {
            mEditText.setText(ddnsIp.trim());
            mEditText.setSelection(ddnsIp.trim().length());
        }
    }


    private OnClickListener mOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.layout_sure:
                    String ddnsIp = mEditText.getText().toString().trim();
                    if (TextUtils.isEmpty(ddnsIp)
                            || CommonUtils.checkIPFormat(ddnsIp)) {
                        DefaultToast.showToast(SettingEditActivity.this,
                                getString(R.string.ip_error));
                        return;
                    }
                    TCService.setServerIp(ddnsIp);
                    DefaultToast.showToast(getApplicationContext(),
                            getString(R.string.save_successful));
                    finish();
                    break;
                case R.id.bottom_back_image:
                    finish();
                    break;

            }
        }
    };
}
